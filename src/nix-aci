#!/bin/sh

set -e
set -x

version=__VERSION__

# only log to stderr
log(){
	printf "%s\n" "$*" >&2;
}

# opts
keep_nix=
verbose=
channel="unstable"
name="nix-container"
output="./out.aci"
input="./default.nix"

# usage
show_help() {
	cat <<-EOF >&2
	nix-aci, version $version
	Usage:
		nix-aci [option] BUILD_DIR
	Arguments:
		BUILD_DIR   : Path to the build directory
	Options:
		-f PATH     : The relative path to the nix expression (default: $input)
		-o PATH     : Set the output path for the image (default: $output)
		-n NAME     : Set the name of the aci (default: $name)
		-c CHANNEL  : Set nixpkgs channel to use (default: $channel)
		-k          : Keep Nix, usually Nix is not included in the output aci        <-- NOTIMP
                              to reduce container size, use this option to retain it.
		-v          : Enable verbose logging
		-h          : Print this help.
	EOF
}

# parse cli args
OPTIND=1
while getopts "h?vkc:n:o:f:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    v)  verbose=1
        ;;
    k)  keep_nix=1
        ;;
    c)  channel=$OPTARG
        ;;
    n)  name=$OPTARG
        ;;
    o)  output=$OPTARG
        ;;
    f)  input=$OPTARG
        ;;
    esac
done
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

build_dir=$1
args="-f $input -n $name -c $channel"

# tar up pkg_dir and send to nix-acbuild container
tar -cf - $build_dir | rkt --insecure-options=image run \
	--stage1-name=coreos.com/rkt/stage1-fly:1.7.0 \
	--interactive \
	__IMAGE_NAME__ -- $args >$output

