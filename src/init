#!/bin/sh

set -e

if [[ "${NIX_REMOTE}" == "daemon" ]]; then
	if [[ -z $NIX_PROFILE ]]; then
		export NIX_PROFILE="/nix/var/nix/profiles/per-container/`hostname`"
	fi
	if [[ -z $NIX_PROFILE ]]; then
		echo "either NIX_PROFILE or a unique hostname must be set"
		exit 1
	fi
	# install packages
	src=/src
	if [[ ! -e $src/default.nix ]]; then
		echo "FATAL: $src/default.nix not found!"
		exit 1
	fi
	echo "fixing permissions for src"
	chgrp -R 30000 /src
	echo "updating channel"
	nix-channel --update
	echo "installing into profile"
	(cd $src && nix-env -f default.nix -i)
	echo "installing coreutils"
	nix-env -i -A nixpkgs.coreutils
	echo "switching to container profile"
	nix-env --switch-profile $NIX_PROFILE
	echo "starting profile heartbeat..."
	(while true; do touch -h $NIX_PROFILE; sleep 60; done) &
	echo "starting app init..."
	init="$HOME/.nix-profile/bin/init"
	if [[ ! -e $init ]]; then
		echo "FATAL: $init not found"
		echo "it is expected that the package in /src creates an 'init' executable"
		exit 1
	fi
	exec $init
else
	# install nix if volume is empty
	if [[ ! -e /nix/store ]]; then
		/install
	fi
	# start garbage collector
	(while true; do /garbage-collector; sleep 3600; done) &
	# start the daemon
	echo "starting nix-daemon"
	if [[ -n $NIX_SHELL ]]; then
		nix-daemon &
		bash
	else
		nix-daemon
	fi
fi
