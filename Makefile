default: all

NIX_VERSION := 1.11.2
NIX_INSTALLER := "nix-$(NIX_VERSION)-x86_64-linux"
NIX_URL := "http://nixos.org/releases/nix/nix-$(NIX_VERSION)/$(NIX_INSTALLER).tar.bz2"
RELEASE := 1
VERSION := $(NIX_VERSION).$(RELEASE)
ARCH := linux-amd64
DAEMON_IMAGE_NAME := nixbox.io/nix-daemon-$(VERSION)
SLAVE_IMAGE_NAME := nixbox.io/nix-$(VERSION)
DAEMON_FILENAME := nix-daemon-$(VERSION)-$(ARCH).aci
SLAVE_FILENAME := nix-$(VERSION)-$(ARCH).aci
NIX_LINK := /nix/var/nix/profiles/default

RKT := rkt --insecure-options=image --debug
TMPDIR=$(HOME)/tmp/

rootfs/bin/busybox:
	mkdir -p $(shell dirname $@)
	wget -N -O $@ https://www.busybox.net/downloads/binaries/latest/busybox-x86_64
	chmod +x $@
	(cd $(shell dirname $@) && for i in $$(./busybox --list); do rm -f $$i; ln -s busybox $$i; done)
	rm -f rootfs/bin/install
	rm -f rootfs/bin/init

rootfs/garbage-collector: src/garbage-collector
	mkdir -p $(shell dirname $@)
	cp $< $@
	chmod +x $@
	
rootfs/init: src/init rootfs/bin/busybox rootfs/etc/passwd rootfs/etc/nix/nix.conf rootfs/etc/group rootfs/install rootfs/etc/resolv.conf rootfs/home/nix rootfs/root rootfs/garbage-collector rootfs/etc/nsswitch.conf rootfs/etc/hosts rootfs/etc/locale.conf rootfs/etc/timezone rootfs/etc/sudoers
	mkdir -p $(shell dirname $@)
	cp $< $@
	chmod +x $@

rootfs/etc/nix/nix.conf: src/nix.conf
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/etc/sudoers: src/sudoers
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/install: src/install rootfs/bin/busybox
	mkdir -p $(shell dirname $@)
	cp $< $@
	chmod +x $@

installer.tar.bz2:
	wget -N -O $@ "$(NIX_URL)"

rootfs/etc/passwd: src/passwd
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/etc/locale.conf: src/locale.conf
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/etc/timezone: src/timezone
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/etc/resolv.conf:
	mkdir -p $(shell dirname $@)
	echo 'nameserver 8.8.8.8' > $@

rootfs/etc/group: src/group
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/etc/nsswitch.conf: src/nsswitch.conf
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/etc/hosts: src/hosts
	mkdir -p $(shell dirname $@)
	cp $< $@

rootfs/home/nix:
	mkdir -p $@
	ln -s /nix/var/nix/profiles/default $@/.nix-profile
	ln -s /nix/.nix-defexpr $@/.nix-defexpr
	echo "https://nixos.org/channels/nixpkgs-unstable nixpkgs" > $@/.nix-channels
	mkdir -p $@/.nixpkgs/
	echo "{ allowUnfree = true; }" > $@/.nixpkgs/config.nix
	chown -R 10001 $@

rootfs/root:
	mkdir -p $@
	ln -s /nix/var/nix/profiles/default $@/.nix-profile
	ln -s /nix/.nix-defexpr $@/.nix-defexpr
	echo "https://nixos.org/channels/nixpkgs-unstable nixpkgs" > $@/.nix-channels
	mkdir -p $@/.nixpkgs/
	echo "{ allowUnfree = true; }" > $@/.nixpkgs/config.nix
	chown -R 0 $@

dist/$(DAEMON_FILENAME): rootfs/init installer.tar.bz2
	mkdir -p $(shell dirname $@)
	rm -rf .acbuild
	acbuild begin ./rootfs
	acbuild set-name "$(DAEMON_IMAGE_NAME)"
	acbuild copy installer.tar.bz2 /installer.tar.bz2
	acbuild environment add HOME "/nix"
	acbuild environment add NIX_VERSION "$(NIX_VERSION)"
	acbuild environment add PATH "/nix/.nix-profile/sbin:/nix/.nix-profile/bin:/bin/"
	acbuild environment add NIX_PATH "nixpkgs=/nix/.nix-defexpr/channels/nixpkgs"
	acbuild environment add SSL_CERT_FILE "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt"
	acbuild environment add PAGER 'less'
	acbuild mount add nix /nix
	acbuild set-exec -- /init
	acbuild write --overwrite $@

dist/$(SLAVE_FILENAME): rootfs/init
	mkdir -p $(shell dirname $@)
	rm -rf .acbuild
	acbuild begin ./rootfs
	acbuild set-name "$(SLAVE_IMAGE_NAME)"
	acbuild environment add HOME "/root"
	acbuild environment add NIX_REMOTE "daemon"
	acbuild environment add NIX_VERSION "$(NIX_VERSION)"
	acbuild environment add PATH "/root/.nix-profile/sbin:/root/.nix-profile/bin:/bin/"
	acbuild environment add NIX_PATH "nixpkgs=/root/.nix-defexpr/channels/nixpkgs"
	acbuild environment add SSL_CERT_FILE "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt"
	acbuild environment add PAGER 'less'
	acbuild mount add nix /nix
	acbuild set-exec -- /nix/var/nix/profiles/default/bin/bash /init
	acbuild write --overwrite $@

dist/$(DAEMON_FILENAME).sha: dist/$(DAEMON_FILENAME)
	$(RKT) fetch $< >$@
	$(RKT) image list | grep $(DAEMON_IMAGE_NAME)

dist/$(SLAVE_FILENAME).sha: dist/$(SLAVE_FILENAME)
	$(RKT) fetch $< >$@
	$(RKT) image list | grep $(SLAVE_IMAGE_NAME)

all: dist/$(DAEMON_FILENAME) dist/$(SLAVE_FILENAME)

run-daemon: dist/$(DAEMON_FILENAME).sha
	mkdir -p $(TMPDIR)/nix
	$(RKT) run --volume nix,kind=host,source=$(TMPDIR)/nix --interactive $(DAEMON_IMAGE_NAME) --set-env NIX_SHELL=true

run-slave: dist/$(SLAVE_FILENAME).sha
	$(RKT) run --interactive --volume nix,kind=host,source=/tmp/nix $(SLAVE_IMAGE_NAME)

test:
	rm -rf dist
	rkt gc --grace-period=0; rm -rf /var/lib/rkt
	make all
	(cd example && make clean run)

release: all
	cp dist/* /mnt/aci.oxdi.eu/nix/

clean:
	rm -rf dist
	rm -rf .acbuild
	rm -rf rootfs

.PHONY: default clean light-clean deploy image sh run-slave run-daemon
