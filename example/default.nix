{ example ? { outPath = ./.; name = "example"; }
, pkgs ? import <nixpkgs> {}
}:
let
  nodePackages = import "${pkgs.path}/pkgs/top-level/node-packages.nix" {
    inherit pkgs;
    inherit (pkgs) stdenv nodejs fetchurl fetchgit;
    neededNatives = [ pkgs.python ] ++ pkgs.lib.optional pkgs.stdenv.isLinux pkgs.utillinux;
    self = nodePackages;
    generated = ./node_modules.nix;
  };
in rec {
  tarball = pkgs.runCommand "example-1.0.0.tgz" { buildInputs = [ pkgs.nodejs ]; } ''
    mv `HOME=$PWD npm pack ${example}` $out
  '';
  build = nodePackages.buildNodePackage {
    name = "example-1.0.0";
    src = [ tarball ];
    buildInputs = nodePackages.nativeDeps."example" or [];
    deps = [ nodePackages.by-spec."chalk"."^1.1.3" ];
    peerDependencies = [];
  };
}
