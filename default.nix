let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
in rec {
  nixImageBuilderEnv = stdenv.mkDerivation rec {
    name = "nix-image-builder-env";
    version = "1.11.2.0";
    src = ./.;
    buildInputs = [ pkgs.gnumake pkgs.acbuild pkgs.rkt ];
    shellHook = ''
      echo "run 'make all' to build acis in ./dest ... 'make release' to copy them to aci.oxdi.eu"
      export PS1="${name} > "
    '';
  };
}
